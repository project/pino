<?php

/**
 * @file
 * Pino profile customizations.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_install_tasks().
 */
function pino_install_tasks(&$install_state) {
  $tasks = [
    'pino_translate_install' => [
      'display_name' => t('Translate installation'),
      'type' => 'normal',
      'display' => FALSE
    ],
    '\Drupal\pino\Form\ConfigurationForm' => [
      'display_name' => t('Additional configuration'),
      'type' => 'form',
    ],
  ];

  return $tasks;
}

/**
 * Implements hook_form_alter().
 */
function pino_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  switch ($form_id) {
    // Alter login form and add own custom submit handler.
    case 'user_login_form':
      $form['#submit'][] = '_pino_user_login_form_submit';
      break;
  }
}

/**
 * Custom submit handler for login form.
 */
function _pino_user_login_form_submit($form, FormStateInterface $form_state) {
  // Redirect the user on login to homepage.
  $form_state->setRedirect('<front>');
}

/**
 * Import custom language files to the installation process.
 */
function pino_translate_install() {

  if(file_exists(drupal_get_path('profile', 'pino') . '/translations/' . $_GET['langcode'] . '.po')) {
    $file = new \stdClass();
    $file->uri = drupal_get_path('profile', 'pino') . '/translations/' . $_GET['langcode'] . '.po';
    $file->langcode = $_GET['langcode'];

    \Drupal\locale\Gettext::fileToDatabase($file, [
      'overwrite_options' => [
        'not_customized' => TRUE,
      ],
    ]);
  }

}

/**
 * Implements hook_css_alter().
 */
function pino_css_alter(&$css, \Drupal\Core\Asset\AttachedAssetsInterface $assets) {

  // Disable the CDN-provided bulma.css that Bulma basetheme provides
  unset($css['https://cdn.jsdelivr.net/npm/bulma@0.5.1/css/bulma.css']);

}

