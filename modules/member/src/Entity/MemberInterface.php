<?php

namespace Drupal\member\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Member entities.
 *
 * @ingroup member
 */
interface MemberInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Member name.
   *
   * @return string
   *   Name of the Member.
   */
  public function getName();

  /**
   * Sets the Member name.
   *
   * @param string $name
   *   The Member name.
   *
   * @return \Drupal\member\Entity\MemberInterface
   *   The called Member entity.
   */
  public function setName($name);

  /**
   * Gets the Member creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Member.
   */
  public function getCreatedTime();

  /**
   * Sets the Member creation timestamp.
   *
   * @param int $timestamp
   *   The Member creation timestamp.
   *
   * @return \Drupal\member\Entity\MemberInterface
   *   The called Member entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Member revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Member revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\member\Entity\MemberInterface
   *   The called Member entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Member revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Member revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\member\Entity\MemberInterface
   *   The called Member entity.
   */
  public function setRevisionUserId($uid);

}
