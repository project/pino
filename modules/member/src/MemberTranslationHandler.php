<?php

namespace Drupal\member;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for member.
 */
class MemberTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
