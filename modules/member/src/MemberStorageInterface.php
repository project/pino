<?php

namespace Drupal\member;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\member\Entity\MemberInterface;

/**
 * Defines the storage handler class for Member entities.
 *
 * This extends the base storage class, adding required special handling for
 * Member entities.
 *
 * @ingroup member
 */
interface MemberStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Member revision IDs for a specific Member.
   *
   * @param \Drupal\member\Entity\MemberInterface $entity
   *   The Member entity.
   *
   * @return int[]
   *   Member revision IDs (in ascending order).
   */
  public function revisionIds(MemberInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Member author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Member revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\member\Entity\MemberInterface $entity
   *   The Member entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(MemberInterface $entity);

  /**
   * Unsets the language for all Member with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
