<?php

namespace Drupal\member_mailer\Form;

use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;

/**
 * Form for mailing members.
 */
class MailMembersForm extends ConfirmFormBase {

  protected $step = 1;

  protected $data = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mail_members_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // For step 2, display the confirmation form.
    if ($this->step == 2) {

      return parent::buildForm($form, $form_state);

    }

    $receivers_options = [
      'active' => t('Active members'),
      'all' => t('All members (including inactive)'),
    ];

    $tids = \Drupal::EntityQuery('taxonomy_term')
      ->condition('vid', 'positions')
      ->execute();

    $terms = Term::loadMultiple($tids);

    foreach ($terms as $term) {

      $receivers_options['tid_' . $term->id()] = t('Active members with position:') . ' ' . $term->getName();

    }

    $form['receivers'] = [
      '#type' => 'select',
      '#title' => t('Receivers'),
      '#required' => TRUE,
      '#options' => $receivers_options,
    ];

    $form['from'] = [
      '#type' => 'email',
      '#title' => t('From address'),
      '#default_value' => \Drupal::currentUser()->getEmail(),
      '#required' => TRUE,
    ];

    $form['subject'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => t('Subject'),
    ];

    $form['message'] = [
      '#required' => TRUE,
      '#type' => 'textarea',
      '#title' => t('Message'),
    ];

    $form['send'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Step 2 requires no validation.
    if ($this->step == 2) {
      return;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // In step 1, rebuild the form, save the values and set the step to 2.
    if ($this->step == 1) {
      $form_state->setRebuild();
      $this->step = 2;
      $this->data = $form_state->getValues();
      return;
    }

    // This is step 2, process the data and create a batch.
    switch (TRUE) {

      case strstr($this->data['receivers'], 'active'):

        $members = \Drupal::EntityQuery('member')
          ->condition('field_is_active', 1)
          ->execute();

        break;

      case strstr($this->data['receivers'], 'all'):

        $members = \Drupal::EntityQuery('member')
          ->execute();

        break;

      case strstr($this->data['receivers'], 'tid_'):

        $tid = str_replace("tid_", "", $this->data['receivers']);

        $members = \Drupal::EntityQuery('member')
          ->condition('field_is_active', 1)
          ->condition('field_position', $tid)
          ->execute();

        break;

    }

    $operations = [];
    foreach ($members as $member) {
      $operations[] = [
        'member_mailer_batch_send', [
          $member,
          $this->data['subject'],
          $this->data['message'],
          $this->data['from'],
        ],
      ];
    }

    $batch = [
      'title' => t('Sending mail to members'),
      'operations' => $operations,
      'finished' => 'member_mailer_batch_send_finished',
      'init_message' => t('Starting to send mail to members.'),
      'progress_message' => t('Processed @current out of @total. Estimated time: @estimate.'),
      'error_message' => t('The mailing process has encountered an error.'),
      'file' => drupal_get_path('module', 'member_mailer') . '/member_mailer.batch.inc',
    ];

    batch_set($batch);

  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('member_mailer.form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {

    switch (TRUE) {

      case strstr($this->data['receivers'], 'active'):

        $member_count = \Drupal::EntityQuery('member')
          ->condition('field_is_active', 1)
          ->count()
          ->execute();

        break;

      case strstr($this->data['receivers'], 'all'):

        $member_count = \Drupal::EntityQuery('member')
          ->count()
          ->execute();

        break;

      case strstr($this->data['receivers'], 'tid_'):

        $tid = str_replace("tid_", "", $this->data['receivers']);

        $member_count = \Drupal::EntityQuery('member')
          ->condition('field_is_active', 1)
          ->condition('field_position', $tid)
          ->count()
          ->execute();

        break;

    }

    return t('Are you sure you want to send the mail %subject to %members members?', [
      '%subject' => $this->data['subject'],
      '%members' => $member_count,
    ]);
  }

}
