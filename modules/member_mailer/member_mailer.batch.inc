<?php

/**
 * @file
 * Handler for member_mailer batches.
 */

use Drupal\member\Entity\Member;

/**
 * Main batch task.
 */
function member_mailer_batch_send($member_id, $subject, $message, $from, &$context) {

  $member = Member::load($member_id);

  $mailManager = \Drupal::service('plugin.manager.mail');

  $params = [
    'subject' => $subject,
    'message' => $message,
    'from' => $from,
  ];

  $mailManager->mail('member_mailer', 'member_mail', $member->get('field_email_address')->value, \Drupal::currentUser()->getPreferredLangcode(), $params, NULL, TRUE);

  $context['results'][] = $member_id;
  $context['message'] = t('Sending mail to @mail', ['@mail' => $member->get('field_email_address')->value]);
}

/**
 * Handler for batch finish.
 */
function member_mailer_batch_send_finished($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    \Drupal::messenger()->addMessage(t('%emails mails were sent successfully.', ['%emails' => count($results)]), 'status');
  }
  else {
    \Drupal::messenger()->addMessage(t('An error occurred and mailing did not complete.'), 'error');
  }

}
