<?php

/**
 * @file
 * Handler for member_import batch importing.
 */

use Drupal\member\Entity\Member;
use Drupal\taxonomy\Entity\Term;

/**
 * Main batch task.
 */
function member_import_batch_import($row, $field_relations, &$context) {

  $entity_field_manager = \Drupal::service('entity_field.manager');

  $field_definitions = $entity_field_manager->getFieldStorageDefinitions('member');

  $member_data = [];

  foreach ($row as $key => $value) {

    if (isset($field_relations[$key]) && $field_relations[$key] != "") {

      // The field is set to be inserted.
      $type = $field_definitions[$field_relations[$key]]->getType();

      // Modify the data if necessary before inserting.
      switch ($type) {
        case 'boolean':
          // Booleans are tricky, let's assume that empty fields are negative.
          if ($value && $value != '') {
            $member_data[$field_relations[$key]] = 1;
          }
          else {
            $member_data[$field_relations[$key]] = 0;
          }
          break;

        case 'datetime':
          $member_data[$field_relations[$key]] = date("Y-m-d", strtotime($value));
          break;

        case 'timestamp':
          $member_data[$field_relations[$key]] = strtotime($value);
          break;

        case 'entity_reference':

          /*
             Let's presume the data is separated by commas
             and that the data is for the position field
          */
          $positions = explode(",",$value);

          foreach($positions as $position) {

            if($position != "") {

              $position = trim($position);

              // Try to load as taxonomy term
              $terms = \Drupal::entityTypeManager()
                ->getStorage('taxonomy_term')
                ->loadByProperties([
                  'name' => $position,
                  'vid' => 'positions',
                ]);

              $term = reset($terms);

              if (!$terms) {

                // Term not found, let's create it
                $term = Term::create([
                  'parent' => [],
                  'vid' => 'positions',
                  'name' => $position,
                ]);

                $term->save();

              }

              $member_data[$field_relations[$key]][] = ['target_id' => $term->id()];
            }
          }

          break;

        default:
          $member_data[$field_relations[$key]] = $value;
          break;
      }

    }

  }

  \Drupal::logger('member_import')->notice(print_r($member_data, TRUE));

  $member = Member::create($member_data);

  $member->save();

  $context['results'][] = $member->id();
  $context['message'] = t('Imported user @user', ['@user' => $member->getName()]);
}

/**
 * Finished function when batch finishes.
 */
function member_import_batch_import_finished($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    \Drupal::messenger()->addMessage(t('%members members were imported succesfully.', ['%members' => count($results)]), 'status');
  }
  else {
    \Drupal::messenger()->addMessage(t('An error occurred and importing did not complete.'), 'error');
  }

}
