<?php

namespace Drupal\pino\Form;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides CSV importer form.
 */
class ConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pino_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = t('Configuring officer account');

    $form['officer_account'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Officer account'),
    ];

    $form['officer_account']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#maxlength' => USERNAME_MAX_LENGTH,
      '#description' => $this->t("Several special characters are allowed, including space, period (.), hyphen (-), apostrophe ('), underscore (_), and the @ sign."),
      '#required' => TRUE,
      '#attributes' => ['class' => ['username']],
    ];

    $form['officer_account']['pass'] = [
      '#type' => 'password_confirm',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['officer_account']['#tree'] = TRUE;
    $form['officer_account']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#required' => TRUE,
    ];

    $form['preferred_date_format'] = [
      '#type' => 'select',
      '#title' => t('Preferred date format'),
      '#description' => t("To be used in member context, for example member's join date"),
      '#options' => [
        'd.m.Y' => date('d.m.Y'),
        'd/m/Y' => date('d/m/Y'),
        'd-m-Y' => date('d-m-Y'),
        'd F Y' => date('d F Y'),
        'm/d/Y' => date('m/d/Y'),
        'F j, Y' => date('F j, Y'),
        'Y-m-d' => date('Y-m-d'),
        'Y.m.d' => date('Y.m.d'),
        'Y/m/d' => date('Y/m/d'),
      ],
    ];

    $form['features'] = [
      '#type' => 'checkboxes',
      '#title' => t('Features (optional)'),
      '#description' => t('Select which features you want to optionally install.'),
      '#options' => [
        'pino_documents' => '<strong>' . t('Documents') . '</strong> - ' . t('Requires working <a href="@private_file_system_url" target="_blank">private file system</a>.', [
          '@private_file_system_url' => 'https://www.drupal.org/docs/8/core/modules/file/overview#content-accessing-private-files'
          ]),
      ],
      '#default_value' => ['pino_documents'],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save and continue'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Username exists already.
    if (user_load_by_name($form_state->getValue(['officer_account', 'name']))) {
      $form_state->setErrorByName('officer_account][name', t('The username %value is already taken.', ['%value' => $form_state->getValue('name')]));
    }

    // Username allowed characters.
    if ($error = user_validate_name($form_state->getValue(['officer_account', 'name']))) {
      $form_state->setErrorByName('officer_account][name', $error);
    }

    // Passwords match.
    if ($form_state->getValue(['officer_account', 'pass', 'pass1']) !== $form_state->getValue([
      'officer_account',
      'pass',
      'pass2',
    ])) {
      $form_state->setErrorByName('officer_account][pass', t('The specified passwords do not match.'));
    }

    // Email exists already.
    if (user_load_by_mail($form_state->getValue(['officer_account', 'mail']))) {
      $form_state->setErrorByName('officer_account][mail', t('The email address %value is already taken.', ['%value' => $form_state->getValue('email')]));
    }

    // Email allowed characters.
    if (!\Drupal::service('email.validator')->isValid($form_state->getValue(['officer_account', 'mail']))) {
      $form_state->setErrorByName('officer_account][mail', t('The email address %mail is not valid.', ['%mail' => $form_state->getValue('email')]));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Create officer user account.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user = User::create();

    $user->setPassword($form_state->getValue(['officer_account', 'pass']));
    $user->enforceIsNew();
    $user->setEmail($form_state->getValue(['officer_account', 'mail']));
    $user->setUsername($form_state->getValue(['officer_account', 'name']));

    $user->set('init', 'email');
    $user->set('langcode', $language);
    $user->set('preferred_langcode', $language);
    $user->set('preferred_admin_langcode', $language);
    $user->activate();

    $user->addRole('officer');

    $user->save();

    // Create default  date format.
    $date_format = DateFormat::create([
      'label' => 'Membership date',
      'id' => 'membership_date',
      'pattern' => $form_state->getValue(['preferred_date_format']),
    ]);

    // Install feature modules
    $installer = \Drupal::service('module_installer');

    foreach($form_state->getValue(['features']) as $feature) {

      $installer->install([$feature]);

    }

    $date_format->save();

  }

}
